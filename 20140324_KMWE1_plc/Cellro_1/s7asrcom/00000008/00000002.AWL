FUNCTION_BLOCK "MATEC 2"
TITLE =MACH_MATEC
AUTHOR : AvdB
VERSION : 0.1


VAR_INPUT
  JOB_DATA : "UDT_JOB_DATA";	//USED JOB_DATA DB
  AckJobM1 : BOOL ;	
  JobBusyM1 : BOOL ;	
  CellInAuto : BOOL ;	
  WINCC_RstMI { S7_m_c := 'true' }: BOOL ;	
  FreigabeRoboter : BOOL ;	
  WerkStuckIO : BOOL ;	
  WerkStuckNIO : BOOL ;	
  PaletteGeklemmt : BOOL ;	
  Spann1Geklemmt : BOOL ;	
  Spann2Geklemmt : BOOL ;	
  LadeturGeschl : BOOL ;	
  LadeturGeoffnt : BOOL ;	
  WerkzEntladen : BOOL ;	
  WerkzBeladen : BOOL ;	
  WerkzWechslen : BOOL ;	
  MagznplzAusgeh : BOOL ;	
  MagznplzGeloest : BOOL ;	
  BearbtnPlztblErlb : BOOL ;	
  WerkzPlatzNummBel : WORD ;	
  WerkzPlatzNummEnt : WORD ;	
END_VAR
VAR_OUTPUT
  ReqJobM1 : BOOL ;	
  AlarmsW : DWORD ;	
  WarningsW : DWORD ;	
  StatusW : DWORD ;	
  NC_Start : BOOL ;	
  PaletteLoesen : BOOL ;	
  Spann1Loesen : BOOL ;	
  Spann2Loesen : BOOL ;	
  TuerSchliessen : BOOL ;	
  TuerOeffnen : BOOL ;	
  RbtAussMach : BOOL ;	
  RbtIstBetriebsBereid : BOOL ;	
  WerkzgGespannt : BOOL ;	
  WerkzEingesetz : BOOL ;	
  WerkzAusgehoben : BOOL ;	
  AnfrgBearbtnPlztbl : BOOL ;	
  WerkzgPlatzNummBel : WORD ;	
  WerkzgPlatzNummEnt : WORD ;	
END_VAR
VAR
  MACHINE : "UDT_MACHINE";	//MACHINE: WINNCC <> PLC machine interface DB
  STATUS : "UDT_STATUS_MACH";	//GENERAL: Negative edges
  Always0 : BOOL ;	//GENERAL: Signal always 0
  Always1 : BOOL ;	//GENERAL: Signal always 1
  PosEdge : ARRAY  [1 .. 40 ] OF //GENERAL: Positive edges
  BOOL ;	
  NegEdge : ARRAY  [1 .. 40 ] OF //GENERAL: Negative edges
  BOOL ;	
  S_RETVAL : ARRAY  [1 .. 10 ] OF //GENERAL: Return values of SFC
  INT ;	
  StepPallet : ARRAY  [0 .. 20 ] OF //MACHINE INTERFACE: Steps for pallet
  BOOL ;	
  MachineNumber : INT ;	
  PalletExhcReady : BOOL ;	
  ToolExhcReady : BOOL ;	
  PalletExhcBusy : BOOL ;	
  ToolHandBusy : BOOL ;	
END_VAR
VAR_TEMP
  T_ClampOccupied : BOOL ;	
  Dummy : BOOL ;	
  GreiferObenRbtIn : BOOL ;	
  GreiferUnterRbtIn : BOOL ;	
  AusgehobenRbtOut : BOOL ;	
END_VAR
BEGIN
NETWORK
TITLE =MACHINE: Copy WINCC communication DB to statics

      CALL "BLKMOV" (
           SRCBLK                   := P#DB501.DBX0.0 BYTE 48,
           RET_VAL                  := #S_RETVAL[1],
           DSTBLK                   := #MACHINE);
      NOP   0; 
NETWORK
TITLE =MACH_MATEC: GENERAL: Signal always 0

      A     #Always0; 
      AN    #Always0; 
      =     #Always0; 
NETWORK
TITLE =MACH_MATEC: GENERAL: Signal always 1

      O     #Always1; 
      ON    #Always1; 
      =     #Always1; 
NETWORK
TITLE =MACH_MATEC: Connect IN variable to static

      A     #Always1; 
      =     L      1.0; 
      A     L      1.0; 
      A     #FreigabeRoboter; 
      =     #MACHINE.MachStartRobot; 
      A     L      1.0; 
      A     #FreigabeRoboter; 
      =     #MACHINE.MachAckRBTenabled; 
      A     L      1.0; 
      A     #LadeturGeoffnt; 
      =     #MACHINE.MachAckDrIsOpen; 
      A     L      1.0; 
      A     #LadeturGeschl; 
      =     #MACHINE.MachAckDrIsClosed; 
      A     L      1.0; 
      AN    #PaletteGeklemmt; 
      =     #MACHINE.MachAckPallClmp1Open; 
      A     L      1.0; 
      A     #PaletteGeklemmt; 
      =     #MACHINE.MachAckPallClmp1Closed; 
      A     L      1.0; 
      A     #PaletteGeklemmt; 
      =     #T_ClampOccupied; 
      A     L      1.0; 
      A     #BearbtnPlztblErlb; 
      =     #MACHINE.MachAckEditToolTable; 
      A     L      1.0; 
      A     #WerkStuckIO; 
      A     #PalletExhcBusy; 
      AN    #NC_Start; 
      =     #MACHINE.MachPrd_OK; 
      A     L      1.0; 
      A     #WerkStuckNIO; 
      A     #PalletExhcBusy; 
      AN    #NC_Start; 
      =     #MACHINE.MachPrd_NOK; 
NETWORK
TITLE =MACH_MATEC: Request to MATEC to edit tool table

      A     #MACHINE.PLCReqEditToolTable; 
      =     #AnfrgBearbtnPlztbl; 
NETWORK
TITLE =MACH_MATEC: PLC>MACH: Robot is betriebsbereid

      A(    ; 
      O     #CellInAuto; 
      O     #Always1; 
      )     ; 
      A     #MACHINE.GenAutoMode; 
      AN    #MACHINE.GenLocalMode; 
      =     #RbtIstBetriebsBereid; 
NETWORK
TITLE =MACH_MATEC: Pallet exchange is busy

      A     #FreigabeRoboter; 
      AN    #WerkzEntladen; 
      AN    #WerkzBeladen; 
      AN    #WerkzWechslen; 
      AN    #ToolHandBusy; 
      S     #PalletExhcBusy; 
      A(    ; 
      O     #PalletExhcReady; 
      ON    #FreigabeRoboter; 
      O     #WINCC_RstMI; 
      )     ; 
      R     #PalletExhcBusy; 
      NOP   0; 
NETWORK
TITLE =MACH_MATEC: Tool handling is busy

      A(    ; 
      O     #WerkzEntladen; 
      O     #WerkzBeladen; 
      O     #WerkzWechslen; 
      )     ; 
      AN    #PalletExhcBusy; 
      A     #FreigabeRoboter; 
      S     #ToolHandBusy; 
      A(    ; 
      O     #ToolExhcReady; 
      O     #WINCC_RstMI; 
      )     ; 
      R     #ToolHandBusy; 
      NOP   0; 
NETWORK
TITLE =MACH_MATEC: MACH>PLC: Machine request for action

      A     #CellInAuto; 
      A     #RbtIstBetriebsBereid; 
      A     #FreigabeRoboter; 
      AN    #MACHINE.MachRequest; 
      AN    #MACHINE.MachReqUnloadTool; 
      AN    #MACHINE.MachReqLoadTool; 
      AN    #MACHINE.MachReqExchTool; 
      AN    #NC_Start; 
      AN    #JobBusyM1; 
      AN    #WerkzgGespannt; 
      AN    #WerkzEingesetz; 
      AN    #WerkzAusgehoben; 
      S     #MACHINE.MachRequest; 
      A(    ; 
      AN    #PalletExhcBusy; 
      AN    #ToolHandBusy; 
      AN    #WerkzEntladen; 
      AN    #WerkzBeladen; 
      AN    #WerkzWechslen; 
      O     #PalletExhcReady; 
      O     #ToolExhcReady; 
      O     #WINCC_RstMI; 
      )     ; 
      R     #MACHINE.MachRequest; 
      NOP   0; 
NETWORK
TITLE =

      A(    ; 
      L     #JOB_DATA.JobNbr; 
      L     50; 
      ==I   ; 
      )     ; 
      A(    ; 
      L     #JOB_DATA.MachineNbr; 
      L     1; 
      ==I   ; 
      )     ; 
      A     #JobBusyM1; 
      A     "RDO_OutOfAreaMach1"; 
      A     #MACHINE.MachRequest; 
      =     #PalletExhcReady; 
NETWORK
TITLE =MACH_MATEC: PLC>MACH: NC start to machine

      A     #PalletExhcReady; 
      S     #NC_Start; 
      AN    #FreigabeRoboter; 
      R     #NC_Start; 
      NOP   0; 
NETWORK
TITLE =MACH_MATEC: MACH>PLC: Request to unload a tool

      A     #MACHINE.MachRequest; 
      A     #WerkzEntladen; 
      AN    #PalletExhcBusy; 
      S     #MACHINE.MachReqUnloadTool; 
      AN    #MACHINE.MachRequest; 
      R     #MACHINE.MachReqUnloadTool; 
      NOP   0; 
NETWORK
TITLE =MACH_MATEC: MACH>PLC: Request to load a tool

      A     #MACHINE.MachRequest; 
      A     #WerkzBeladen; 
      AN    #PalletExhcBusy; 
      S     #MACHINE.MachReqLoadTool; 
      AN    #MACHINE.MachRequest; 
      R     #MACHINE.MachReqLoadTool; 
      NOP   0; 
NETWORK
TITLE =MACH_MATEC: MACH>PLC: Request to exchange a tool

      A     #MACHINE.MachRequest; 
      A     #WerkzWechslen; 
      AN    #PalletExhcBusy; 
      S     #MACHINE.MachReqExchTool; 
      AN    #MACHINE.MachRequest; 
      R     #MACHINE.MachReqExchTool; 
      NOP   0; 
NETWORK
TITLE =MACH_MATEC: MACH>PLC: Copy tool unloading number PLC<>WINCC

      AN    #MACHINE.MachRequest; 
      A     "M0.0 Always0"; 
      JNB   _001; 
      L     #WerkzPlatzNummEnt; 
      T     #MACHINE.MachUnlTlNbr; 
_001: NOP   0; 
NETWORK
TITLE =MACH_MATEC: MACH>PLC: Copy tool loading number PLC<>WINCC

      AN    #MACHINE.MachRequest; 
      A     "M0.0 Always0"; 
      JNB   _002; 
      L     #WerkzPlatzNummBel; 
      T     #MACHINE.MachLdTlNbr; 
_002: NOP   0; 
NETWORK
TITLE =



NETWORK
TITLE =MACH_MATEC: MACH>PLC: Copy tool unloading number PLC<>WINCC

      AN    #MACHINE.MachRequest; 
      JNB   _009; 
      L     #WerkzPlatzNummEnt; 
      CAW   ; 
      T     #MACHINE.MachUnlTlNbr; 
_009: NOP   0; 
NETWORK
TITLE =MACH_MATEC: MACH>PLC: Copy tool loading number PLC<>WINCC

      AN    #MACHINE.MachRequest; 
      JNB   _00a; 
      L     #WerkzPlatzNummBel; 
      CAW   ; 
      T     #MACHINE.MachLdTlNbr; 
_00a: NOP   0; 
NETWORK
TITLE =

NETWORK
TITLE =MACH_MATEC: PLC>MACH: Copy numbers back to machine

      O     #WerkzEntladen; 
      O     #WerkzBeladen; 
      O     #WerkzWechslen; 
      =     L      1.0; 
      A     L      1.0; 
      JNB   _003; 
      L     #MACHINE.MachUnlTlNbr; 
      T     #WerkzgPlatzNummEnt; 
_003: NOP   0; 
      A     L      1.0; 
      JNB   _004; 
      L     #MACHINE.MachLdTlNbr; 
      T     #WerkzgPlatzNummBel; 
_004: NOP   0; 
NETWORK
TITLE =MACH_MATEC: PLC>MACH: Set numbers back to 0

      A(    ; 
      L     #JOB_DATA.JobNbr; 
      L     6; 
      ==I   ; 
      )     ; 
      A(    ; 
      L     #JOB_DATA.MachineNbr; 
      L     1; 
      ==I   ; 
      )     ; 
      A(    ; 
      L     #WerkzPlatzNummEnt; 
      L     0; 
      ==I   ; 
      )     ; 
      A(    ; 
      L     #WerkzPlatzNummBel; 
      L     0; 
      ==I   ; 
      )     ; 
      AN    #FreigabeRoboter; 
      AN    #WerkzEntladen; 
      AN    #WerkzBeladen; 
      AN    #WerkzWechslen; 
      A     #MACHINE.MachRequest; 
      =     L      1.0; 
      A     L      1.0; 
      JNB   _005; 
      L     0; 
      T     #MACHINE.MachUnlTlNbr; 
_005: NOP   0; 
      A     L      1.0; 
      JNB   _006; 
      L     #MACHINE.MachUnlTlNbr; 
      T     #WerkzgPlatzNummEnt; 
_006: NOP   0; 
      A     L      1.0; 
      JNB   _007; 
      L     0; 
      T     #MACHINE.MachLdTlNbr; 
_007: NOP   0; 
      A     L      1.0; 
      JNB   _008; 
      L     #MACHINE.MachLdTlNbr; 
      T     #WerkzgPlatzNummBel; 
_008: NOP   0; 
      A     L      1.0; 
      BLD   102; 
      =     #ToolExhcReady; 
NETWORK
TITLE =MACH_MATEC: INTERFACING SIGNALS PLC <> WINCC

      A     #MACHINE.MachRequest; 
      A     #MACHINE.GenAutoMode; 
      AN    #AckJobM1; 
      AN    #JobBusyM1; 
      =     #ReqJobM1; 
NETWORK
TITLE =



NETWORK
TITLE =COLLECT ROBOT SIGNALS

      A     "RDO_ReqEntMach1"; 
      A     "RDO_ReqDrOpn"; 
      A     "RDO_ReqDrClse"; 
      A     "RDO_ReqClmp1Op"; 
      A     "RDO_ReqClmp1Cls"; 
      A     "RDO_ReqClmp2Op"; 
      A     "RDO_ReqClmp2Cls"; 
      A     "RDO_ReqTlClmpOp"; 
      A     "RDO_ReqTlClmpCl"; 
      A     "RDO_OutOfAreaMach1"; 
      O     ; 
      A     "RDI_AckEntMach1"; 
      A     "RDI_AckDrOpn"; 
      A     "RDI_AckDrClse"; 
      A     "RDI_AckClmp1Op"; 
      A     "RDI_AckClmp1Cls"; 
      A     "RDI_AckClmp2Op"; 
      A     "RDI_AckClmp2Cls"; 
      A     "RDI_AckTlClmpOp"; 
      A     "RDI_AckTlClmpCl"; 
      A     "RDI_AckClmpAirOn"; 
      =     #Dummy; 
NETWORK
TITLE =ROBOT DIxx: Acknowledge to enter Machine 1

      A     "RDO_ReqEntMach1"; 
      A(    ; 
      A(    ; 
      O(    ; 
      L     #JOB_DATA.JobNbr; 
      L     5; 
      ==I   ; 
      )     ; 
      O(    ; 
      L     #JOB_DATA.JobNbr; 
      L     6; 
      ==I   ; 
      )     ; 
      O(    ; 
      L     #JOB_DATA.JobNbr; 
      L     15; 
      ==I   ; 
      )     ; 
      O(    ; 
      L     #JOB_DATA.JobNbr; 
      L     16; 
      ==I   ; 
      )     ; 
      )     ; 
      A(    ; 
      L     #JOB_DATA.MachineNbr; 
      L     1; 
      ==I   ; 
      )     ; 
      A     #JobBusyM1; 
      O     #LadeturGeoffnt; 
      )     ; 
      =     "RDI_AckEntMach1"; 
NETWORK
TITLE =ROBOT DOxx: Robot out of area machine 1

      A     "RDO_OutOfAreaMach1"; 
      =     #RbtAussMach; 
NETWORK
TITLE =ROBOT DIxx: Acknowledge door is open

      A     "RDO_ReqDrOpn"; 
      A     #LadeturGeoffnt; 
      =     "RDI_AckDrOpn"; 
NETWORK
TITLE =ROBOT DIxx: Acknowledge door is closed

      A     "RDO_ReqDrClse"; 
      A     #LadeturGeschl; 
      =     "RDI_AckDrClse"; 
NETWORK
TITLE =MACH_MATEC: PLC>MACH: open/close machine door

      A(    ; 
      A     "RDO_ReqDrOpn"; 
      S     #TuerOeffnen; 
      A     "RDO_ReqDrClse"; 
      R     #TuerOeffnen; 
      A     #TuerOeffnen; 
      )     ; 
      NOT   ; 
      =     #TuerSchliessen; 
NETWORK
TITLE =ROBOT DIxx: Acknowledge clamp 1 is open

      A     "RDO_ReqClmp1Op"; 
      AN    #PaletteGeklemmt; 
      =     "RDI_AckClmp1Op"; 
NETWORK
TITLE =ROBOT DIxx: Acknowledge clamp 1 is closed

      A     "RDO_ReqClmp1Cls"; 
      A     #PaletteGeklemmt; 
      =     "RDI_AckClmp1Cls"; 
NETWORK
TITLE =MACH_MATEC: PLC>MACH: open/close clamp system 1

      A     "RDO_ReqClmp1Op"; 
      S     #PaletteLoesen; 
      A     "RDO_ReqClmp1Cls"; 
      R     #PaletteLoesen; 
      NOP   0; 
NETWORK
TITLE =ROBOT DIxx: Acknowledge clamp 2 is open

      A     "RDO_ReqClmp2Op"; 
      AN    #Spann1Geklemmt; 
      =     "RDI_AckClmp2Op"; 
NETWORK
TITLE =ROBOT DIxx: Acknowledge clamp 2 is closed

      A     "RDO_ReqClmp2Cls"; 
      A     #Spann1Geklemmt; 
      =     "RDI_AckClmp2Cls"; 
NETWORK
TITLE =MACH_MATEC: PLC>MACH: open/close clamp system 2

      A     "RDO_ReqClmp2Op"; 
      S     #Spann1Loesen; 
      A     "RDO_ReqClmp2Cls"; 
      R     #Spann1Loesen; 
      NOP   0; 
NETWORK
TITLE =Needed IN/OUT robot

      A     #GreiferObenRbtIn; 
      A     #GreiferUnterRbtIn; 
      O     #AusgehobenRbtOut; 
      =     #Dummy; 
NETWORK
TITLE =ROBOT DIxx: Acknowledge tool clamp is open

      A(    ; 
      O     #WerkzEntladen; 
      O     #WerkzBeladen; 
      O     #WerkzWechslen; 
      )     ; 
      A     #WerkzAusgehoben; 
      A     #MagznplzGeloest; 
      A     #GreiferObenRbtIn; 
      A     "RDO_ReqTlClmpOp"; 
      =     "RDI_AckTlClmpOp"; 
NETWORK
TITLE =ROBOT DIxx: Acknowledge tool clamp is closed

      A(    ; 
      O     #WerkzEntladen; 
      O     #WerkzBeladen; 
      O     #WerkzWechslen; 
      )     ; 
      A     #WerkzAusgehoben; 
      AN    #MagznplzGeloest; 
      A     #GreiferObenRbtIn; 
      A     "RDO_ReqTlClmpCl"; 
      =     "RDI_AckTlClmpCl"; 
NETWORK
TITLE =Machine OUT: WerkzgGespannt

      A(    ; 
      A     #WerkzBeladen; 
      A     #WerkzAusgehoben; 
      AN    "RDO_ReqTlClmpCl"; 
      O     ; 
      A(    ; 
      O     #WerkzEntladen; 
      O     #WerkzWechslen; 
      )     ; 
      A     #WerkzAusgehoben; 
      A     #GreiferObenRbtIn; 
      A     "RDO_ReqTlClmpOp"; 
      )     ; 
      S     #WerkzgGespannt; 
      A(    ; 
      A     #WerkzBeladen; 
      A     #GreiferObenRbtIn; 
      AN    #MagznplzGeloest; 
      AN    "RDO_ReqTlClmpCl"; 
      O     ; 
      A(    ; 
      O     #WerkzEntladen; 
      O     #WerkzWechslen; 
      )     ; 
      A     #WerkzgGespannt; 
      AN    "RDO_ReqTlClmpCl"; 
      O     #WINCC_RstMI; 
      )     ; 
      R     #WerkzgGespannt; 
      NOP   0; 
NETWORK
TITLE =



NETWORK
TITLE =MACH_MATEC: Connect OUT variable to static

      A     #Always1; 
      =     L      1.0; 
      A     L      1.0; 
      A     #NC_Start; 
      =     #MACHINE.PLCStartMachine; 
      A     L      1.0; 
      A     #RbtAussMach; 
      =     #MACHINE.PLCRbtOutOfArea; 
      A     L      1.0; 
      A     #PaletteLoesen; 
      =     #MACHINE.PLCReqOpenPallClmp1; 
      A     L      1.0; 
      AN    #PaletteLoesen; 
      =     #MACHINE.PLCReqClosePallClmp1; 
      A     L      1.0; 
      A     #Always0; 
      =     #MACHINE.PLCReqAirBlowerOn; 
      A     L      1.0; 
      A     #TuerOeffnen; 
      =     #MACHINE.PLCReqMachArea; 
NETWORK
TITLE =MACH_MATEC: Connect INPUTS to DB for status view in WINCC

      A     #Always1; 
      =     L      1.0; 
      A     L      1.0; 
      A     #Always0; 
      =     #MACHINE.InpPallClamp1_Open; 
      A     L      1.0; 
      A     #Always0; 
      =     #MACHINE.InpPallClamp1_Clsd; 
      A     L      1.0; 
      A     #Always0; 
      =     #MACHINE.InpPallClamp1_Occ; 
NETWORK
TITLE =MACHINE: ALARMS / WARNINGS / STATUS



NETWORK
TITLE =MACHINE ALARM 1: Spare 

      A     #Always0; 
      =     #STATUS.ALARMS.ALARM_1; 
NETWORK
TITLE =MACHINE ALARM 2: Robot movement in machine and no machine enable

      A(    ; 
      L     #MachineNumber; 
      L     #MachineNumber; 
      ==I   ; 
      )     ; 
      AN    "RDO_HomePos"; 
      AN    #Always1; 
      AN    "RDO_HomePos"; 
      =     #STATUS.ALARMS.ALARM_2; 
NETWORK
TITLE =MACHINE ALARM 3: Spare 

      A     #Always0; 
      =     #STATUS.ALARMS.ALARM_3; 
NETWORK
TITLE =MACHINE ALARM 4: Spare 

      A     #Always0; 
      =     #STATUS.ALARMS.ALARM_4; 
NETWORK
TITLE =MACHINE ALARM 5: Spare 

      A     #Always0; 
      =     #STATUS.ALARMS.ALARM_5; 
NETWORK
TITLE =MACHINE WARNING 1: Spare 

      A     #Always0; 
      =     #STATUS.WARNINGS.WARNING_1; 
NETWORK
TITLE =MACHINE STATUS 1: Spare 

      A     #Always0; 
      =     #STATUS.STATUS.STATUS_1; 
NETWORK
TITLE =WRITE MACHINE WINCC DATA: Copy ALARMS to OUT parameter

      CALL "BLKMOV" (
           SRCBLK                   := #STATUS.ALARMS,
           RET_VAL                  := #S_RETVAL[6],
           DSTBLK                   := #AlarmsW);
      NOP   0; 
NETWORK
TITLE =WRITE MACHINE WINCC DATA: Copy WARNINGS to OUT parameter

      CALL "BLKMOV" (
           SRCBLK                   := #STATUS.WARNINGS,
           RET_VAL                  := #S_RETVAL[7],
           DSTBLK                   := #WarningsW);
      NOP   0; 
NETWORK
TITLE =WRITE MACHINE WINCC DATA: Copy STATUS to OUT parameter

      CALL "BLKMOV" (
           SRCBLK                   := #STATUS.STATUS,
           RET_VAL                  := #S_RETVAL[8],
           DSTBLK                   := #StatusW);
      NOP   0; 
NETWORK
TITLE =MACHINE ALARM generated in FB: Program error in machine FB

      O(    ; 
      L     #S_RETVAL[1]; 
      L     0; 
      <>I   ; 
      )     ; 
      O(    ; 
      L     #S_RETVAL[2]; 
      L     0; 
      <>I   ; 
      )     ; 
      O(    ; 
      L     #S_RETVAL[3]; 
      L     0; 
      <>I   ; 
      )     ; 
      O(    ; 
      L     #S_RETVAL[4]; 
      L     0; 
      <>I   ; 
      )     ; 
      O(    ; 
      L     #S_RETVAL[5]; 
      L     0; 
      <>I   ; 
      )     ; 
      O(    ; 
      L     #S_RETVAL[6]; 
      L     0; 
      <>I   ; 
      )     ; 
      O(    ; 
      L     #S_RETVAL[7]; 
      L     0; 
      <>I   ; 
      )     ; 
      O(    ; 
      L     #S_RETVAL[8]; 
      L     0; 
      <>I   ; 
      )     ; 
      =     #STATUS.ALARMS.ALARM_32; 
NETWORK
TITLE =MACHINE: Copy statics to WINCC communication DB

      CALL "BLKMOV" (
           SRCBLK                   := #MACHINE,
           RET_VAL                  := #S_RETVAL[1],
           DSTBLK                   := P#DB501.DBX0.0 BYTE 48);
      NOP   0; 
END_FUNCTION_BLOCK

